SHELL = /bin/bash

build_tag ?= iipimage-server

iipsrv_version = 1.2
iipsrv_commit = d978bfde03955150bbc9fee37836d3fa52763a7a
iipsrv_sha256 = ef7427b790f427654429aa2a198998f22df1026b9698746070ba400095033626

app_version = $(iipsrv_version)+$(iipsrv_commit)

chart_path = ./chart/iipimage-server
chart_name = $(shell cat $(chart_path)/Chart.yaml | grep ^name | awk '{print $$2}')
chart_version = $(shell cat $(chart_path)/Chart.yaml | grep ^version | awk '{print $$2}')
chart_package = $(chart_name)-$(chart_version).tgz

.PHONY: build
build:
	DOCKER_BUILDKIT=1 docker build --pull -t $(build_tag) \
	    --build-arg iipsrv_version=$(iipsrv_version) \
		--build-arg iipsrv_commit=$(iipsrv_commit) \
		--build-arg iipsrv_sha256=$(iipsrv_sha256) \
		- < ./Dockerfile

.PHONY: test
test:
	./test/run

.PHONY: lint-chart
lint-chart:
	helm lint $(chart_path)

.PHONY: update-chart-deps
update-chart-deps:
	helm dependency update $(chart_path)

.PHONY: clean-chart
clean-chart:
	rm -f ./$(chart_name)-*.tgz

$(chart_package): clean-chart
	helm package -u $(chart_path)

.PHONY: package-chart
package-chart: $(chart_package)

ifdef CI
.PHONY: helm-login
helm-login:
	helm registry login -u gitlab-ci-token -p $(CI_JOB_TOKEN) $(CI_REGISTRY)

.PHONY: chart
chart: $(chart_package) helm-login
	helm push $(chart_package) oci://$(CI_REGISTRY_IMAGE)/chart
endif
