# IIPImage server container image

This image is built from source files downloaded from the official repository on GitHub.

## Requirements

- Docker + docker-compose (Docker Desktop or Rancher Desktop for Mac/Windows)
- GNU Make

## Usage

    $ docker pull gitlab-registry.oit.duke.edu/devops/containers/iipimage-server:main
    $ docker run --rm gitlab-registry.oit.duke.edu/devops/containers/iipimage-server:main

The default command runs the FCGI script and binds to port 9000.

## Build

    $ make

## Full stack

    $ docker-compose up -d

Go to `http://localhost:8080/` in your browser to view the test images.

Stop the stack with:

    $ docker-compose down
